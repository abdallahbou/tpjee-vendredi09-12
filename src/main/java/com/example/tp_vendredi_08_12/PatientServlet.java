package com.example.tp_vendredi_08_12;

import com.example.tp_vendredi_08_12.entity.Patient;
import com.example.tp_vendredi_08_12.services.PatientService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "patientServlet", value = "/patient")
public class PatientServlet extends HttpServlet {

    private List<Patient> patientList;


    private PatientService patientService;


    public void init() {
        patientService = new PatientService();
       patientList = patientService.allPatient();
    }



   public void doGet( HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {


        if (request.getParameter("id") != null) {

            int id = Integer.parseInt(request.getParameter("id"));
            Patient patient = new Patient();
            try {
                patient = patientService.patientId(id);
            } catch (Exception e) {

            }
            if (patient != null) {

                request.setAttribute("patient", patient);
                request.getRequestDispatcher("patient2.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("<div> aucun produit avec cette id</div>");
            }
        } else {
            request.setAttribute("patientList", patientList);
            request.getRequestDispatcher("patient1").forward(request, response);
        }

    }


}
