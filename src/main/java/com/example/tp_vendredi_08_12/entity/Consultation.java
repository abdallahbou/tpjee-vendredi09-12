package com.example.tp_vendredi_08_12.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Consultation {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idConsultation;

    private Date dateConsultation;

    private String heure;

    private String lieu;

    private char etatConsult;

    @OneToOne(mappedBy = "consultation")
    private FicheConsultation ficheConsultation;

    public Consultation() {
    }

    public int getIdConsultation() {
        return idConsultation;
    }

    public void setIdConsultation(int idConsultation) {
        this.idConsultation = idConsultation;
    }

    public Date getDateConsultation() {
        return dateConsultation;
    }

    public void setDateConsultation(Date dateConsultation) {
        this.dateConsultation = dateConsultation;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public char getEtatConsult() {
        return etatConsult;
    }

    public void setEtatConsult(char etatConsult) {
        this.etatConsult = etatConsult;
    }

    public FicheConsultation getFicheConsultation() {
        return ficheConsultation;
    }

    public void setFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultation = ficheConsultation;
    }
}
