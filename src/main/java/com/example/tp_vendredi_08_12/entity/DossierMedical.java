package com.example.tp_vendredi_08_12.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class DossierMedical {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idDm;

    private int numero;

    private Date dateDeCreation;

    private String codeAccessPatient;

    @OneToOne(mappedBy = "dossierMedical")
    private Patient patient;

    @OneToMany(mappedBy = "dossierMedical")
    private List<FicheDeSoin> ficheDeSoins = new ArrayList<>();


    public DossierMedical() {
    }

    public DossierMedical(int numero, String codeAccessPatient) {
        this.numero = numero;
        this.codeAccessPatient = codeAccessPatient;
    }

    public int getIdDm() {
        return idDm;
    }

    public void setIdDm(int idDm) {
        this.idDm = idDm;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Date getDateDeCreation() {
        return dateDeCreation;
    }

    public void setDateDeCreation(Date dateDeCreation) {
        this.dateDeCreation = dateDeCreation;
    }

    public String getCodeAccessPatient() {
        return codeAccessPatient;
    }

    public void setCodeAccessPatient(String codeAccessPatient) {
        this.codeAccessPatient = codeAccessPatient;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public List<FicheDeSoin> getFicheDeSoins() {
        return ficheDeSoins;
    }

    public void setFicheDeSoins(List<FicheDeSoin> ficheDeSoins) {
        this.ficheDeSoins = ficheDeSoins;
    }
}
