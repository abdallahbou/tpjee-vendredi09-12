package com.example.tp_vendredi_08_12.entity;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
public class FicheConsultation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idFc;

    private String compteRendu;

    @OneToMany(mappedBy = "ficheConsultation")
    private Set<Prescription> prescriptionSet;

    @OneToMany(mappedBy = "ficheConsultation")
    private Set<OperationAnalyse> analyseset;

    @OneToOne
    private Consultation consultation;


    @ManyToMany
    @JoinTable(name = "fiche",joinColumns = @JoinColumn(name = "id_consult"),
    inverseJoinColumns = @JoinColumn(name = "id_soin"))
    private Set<FicheDeSoin> ficheDeSoinSet;

    public FicheConsultation() {
    }

    public int getIdFc() {
        return idFc;
    }

    public void setIdFc(int idFc) {
        this.idFc = idFc;
    }

    public String getCompteRendu() {
        return compteRendu;
    }

    public void setCompteRendu(String compteRendu) {
        this.compteRendu = compteRendu;
    }


    public Set<Prescription> getPrescriptionSet() {
        return prescriptionSet;
    }

    public void setPrescriptionSet(Set<Prescription> prescriptionSet) {
        this.prescriptionSet = prescriptionSet;
    }

    public Set<OperationAnalyse> getAnalyseset() {
        return analyseset;
    }

    public void setAnalyseset(Set<OperationAnalyse> analyseset) {
        this.analyseset = analyseset;
    }

    public Set<FicheDeSoin> getFicheDeSoinSet() {
        return ficheDeSoinSet;
    }

    public void setFicheDeSoinSet(Set<FicheDeSoin> ficheDeSoinSet) {
        this.ficheDeSoinSet = ficheDeSoinSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FicheConsultation that = (FicheConsultation) o;
        return idFc == that.idFc && Objects.equals(compteRendu, that.compteRendu) && Objects.equals(ficheDeSoinSet, that.ficheDeSoinSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idFc, compteRendu, ficheDeSoinSet);
    }
}
