package com.example.tp_vendredi_08_12.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class FicheDePayement extends FicheDeSoin  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idFp;

    private Date dateExigibilite;

    private Date datePayement;

    private double montantPaye;

    private boolean indicateurPayement;

//    @ManyToMany(cascade = CascadeType.PERSIST)
//    @JoinTable(name = "payement",joinColumns = @JoinColumn(name="paye_id"),
//    inverseJoinColumns = @JoinColumn(name="soin_id"))
//    private List<FicheDeSoin> ficheDeSoins =new ArrayList<>();

    public FicheDePayement() {
    }

    public int getIdFp() {
        return idFp;
    }

    public void setIdFp(int idFp) {
        this.idFp = idFp;
    }

    public Date getDateExigibilite() {
        return dateExigibilite;
    }

    public void setDateExigibilite(Date dateExigibilite) {
        this.dateExigibilite = dateExigibilite;
    }

    public Date getDatePayement() {
        return datePayement;
    }

    public void setDatePayement(Date datePayement) {
        this.datePayement = datePayement;
    }

    public double getMontantPaye() {
        return montantPaye;
    }

    public void setMontantPaye(double montantPaye) {
        this.montantPaye = montantPaye;
    }

    public boolean isIndicateurPayement() {
        return indicateurPayement;
    }

    public void setIndicateurPayement(boolean indicateurPayement) {
        this.indicateurPayement = indicateurPayement;
    }

//    public List<FicheDeSoin> getFicheDeSoins() {
//        return ficheDeSoins;
//    }
//
//    public void setFicheDeSoins(List<FicheDeSoin> ficheDeSoins) {
//        this.ficheDeSoins = ficheDeSoins;
//    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        FicheDePayement that = (FicheDePayement) o;
//        return idFp == that.idFp && Double.compare(that.montantPaye, montantPaye) == 0 && indicateurPayement == that.indicateurPayement && Objects.equals(dateExigibilite, that.dateExigibilite) && Objects.equals(datePayement, that.datePayement) && Objects.equals(ficheDeSoins, that.ficheDeSoins);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(idFp, dateExigibilite, datePayement, montantPaye, indicateurPayement, ficheDeSoins);
//    }
}
