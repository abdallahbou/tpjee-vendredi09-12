package com.example.tp_vendredi_08_12.entity;

import javax.persistence.*;
import java.util.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class FicheDeSoin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idfs;

    private int numeroFiche;

    private Date dateCreation;

    private String agentCreateur;

    private String adresseCreateur;


    @ManyToMany(mappedBy = "ficheDeSoins")
    private List<FicheDePayement> ficheDePayementList=new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "id_dossier")
    private DossierMedical dossierMedical;

    @ManyToMany(mappedBy = "ficheDeSoinSet")
    private Set<FicheConsultation> ficheConsultationSet;


    public FicheDeSoin() {
    }

    public int getIdfs() {
        return idfs;
    }

    public void setIdfs(int idfs) {
        this.idfs = idfs;
    }

    public int getNumeroFiche() {
        return numeroFiche;
    }

    public void setNumeroFiche(int numeroFiche) {
        this.numeroFiche = numeroFiche;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getAgentCreateur() {
        return agentCreateur;
    }

    public void setAgentCreateur(String agentCreateur) {
        this.agentCreateur = agentCreateur;
    }

    public String getAdresseCreateur() {
        return adresseCreateur;
    }

    public void setAdresseCreateur(String adresseCreateur) {
        this.adresseCreateur = adresseCreateur;
    }

    public List<FicheDePayement> getFicheDePayementList() {
        return ficheDePayementList;
    }

    public void setFicheDePayementList(List<FicheDePayement> ficheDePayementList) {
        this.ficheDePayementList = ficheDePayementList;
    }

    public DossierMedical getDossierMedical() {
        return dossierMedical;
    }

    public void setDossierMedical(DossierMedical dossierMedical) {
        this.dossierMedical = dossierMedical;
    }

    public Set<FicheConsultation> getFicheConsultationSet() {
        return ficheConsultationSet;
    }

    public void setFicheConsultationSet(Set<FicheConsultation> ficheConsultationSet) {
        this.ficheConsultationSet = ficheConsultationSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FicheDeSoin that = (FicheDeSoin) o;
        return idfs == that.idfs && numeroFiche == that.numeroFiche && Objects.equals(dateCreation, that.dateCreation) && Objects.equals(agentCreateur, that.agentCreateur) && Objects.equals(adresseCreateur, that.adresseCreateur) && Objects.equals(ficheDePayementList, that.ficheDePayementList) && Objects.equals(dossierMedical, that.dossierMedical) && Objects.equals(ficheConsultationSet, that.ficheConsultationSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idfs, numeroFiche, dateCreation, agentCreateur, adresseCreateur, ficheDePayementList, dossierMedical, ficheConsultationSet);
    }
}
