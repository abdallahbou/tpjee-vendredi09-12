package com.example.tp_vendredi_08_12.entity;

import javax.persistence.*;
import java.util.Date;


@Entity
public class OperationAnalyse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idOp;

    private String description;

    private Date dateHeureOperation;

    private String resultat;

@ManyToOne
private  FicheConsultation ficheConsultation;

    public OperationAnalyse() {
    }

    public int getIdOp() {
        return idOp;
    }

    public void setIdOp(int idOp) {
        this.idOp = idOp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateHeureOperation() {
        return dateHeureOperation;
    }

    public void setDateHeureOperation(Date dateHeureOperation) {
        this.dateHeureOperation = dateHeureOperation;
    }

    public String getResultat() {
        return resultat;
    }

    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    public FicheConsultation getFicheConsultation() {
        return ficheConsultation;
    }

    public void setFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultation = ficheConsultation;
    }
}

