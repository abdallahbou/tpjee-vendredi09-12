package com.example.tp_vendredi_08_12.interfaces;

import com.example.tp_vendredi_08_12.entity.Patient;

import java.util.List;

public interface PatientIDAO {

    Patient createPatient(Patient e);

    Patient patientId(int id);

    List<Patient> allPatient();

}
