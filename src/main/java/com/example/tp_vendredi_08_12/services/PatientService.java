package com.example.tp_vendredi_08_12.services;

import com.example.tp_vendredi_08_12.entity.Patient;
import com.example.tp_vendredi_08_12.interfaces.PatientIDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.util.List;


public class PatientService implements PatientIDAO {

    private StandardServiceRegistry registre;

    private SessionFactory sessionFactory;

    private Session session;
    public PatientService(){

        registre = new StandardServiceRegistryBuilder().configure().build();
        sessionFactory = new MetadataSources(registre).buildMetadata().buildSessionFactory();
        session = sessionFactory.openSession();

    }

    @Override
    public Patient createPatient(Patient e) {

        session= sessionFactory.openSession();
        session.beginTransaction();
        session.save(e);
        session.getTransaction().commit();
        return e;
    }

    @Override
    public Patient patientId(int id) {
        Patient patient;
        session = sessionFactory.openSession();
        session.beginTransaction();
        patient = session.get(Patient.class,id);
        session.getTransaction().commit();

        return patient;
    }

    @Override
    public List<Patient> allPatient() {
        session = sessionFactory.openSession();
        session.beginTransaction();
        Query<Patient> patientQuery = session.createQuery("from Patient");
        session.getTransaction().commit();
        return patientQuery.list();
    }
}
